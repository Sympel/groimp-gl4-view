# OpenGL4-based 3D-View for GroIMP

This plugin provides an OpenGL 4 based 3D-view for GroIMP, which aims to be a replacement to for the outdated 3d views based on OpenGL 1 currently used in GroIMP.
