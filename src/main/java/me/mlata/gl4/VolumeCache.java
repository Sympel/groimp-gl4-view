package me.mlata.gl4;

import de.grogra.graph.Cache;
import de.grogra.graph.ContextDependent;
import de.grogra.graph.GraphState;
import de.grogra.imp3d.LineArray;
import de.grogra.imp3d.LineSegmentizable;
import de.grogra.imp3d.PolygonArray;
import de.grogra.imp3d.Polygonizable;
import de.grogra.imp3d.Polygonization;
import me.mlata.gl4.volumes.Lines;
import me.mlata.gl4.volumes.Polygons;
import me.mlata.gl4.volumes.Volume;

/**
 * A cache for {@link Volume Volumes} created from either a {@link LineSegmentizable} or
 * a {@link Polygonizable}.
 */
public class VolumeCache extends Cache {
	final int flags;
	final float flatness;
	// TODO: actually use shareLists
	final boolean shareLists;

	/**
	 * Constructs a new cache. The parameters will be passed
	 * to {@link Polygonization#polygonize}
	 * when a cache entry is to be computed.
	 * 
	 * @param gs the graph state within which the cache will be used
	 * @param flags the flags to pass to <code>polygonize</code>
	 * @param flatness the flatness to pass to <code>polygonize</code>
	 * @param shareLists shall the {@link PolygonArray}s behind the
     * returned {@link VertexArrayObject}s share their lists
	 * (<code>vertices</code>, <code>normals</code> etc.)? This is useful where the
	 * returned data is copied into another representation and no longer needed
	 */
	public VolumeCache(GraphState gs, int flags, float flatness, boolean shareLists) {
		super(gs);
		this.flags = flags;
		this.flatness = flatness;
		this.shareLists = shareLists;
	}


	/**
	 * Returns the {@link Volume} of a {@link Polygonizable}
	 * in the given object context.
	 * 
	 * @param object the context
	 * @param asNode <code>true</code> if <code>object</code> is a node,
	 * <code>false</code> if <code>object</code> is an edge
	 * @param p the polygonizable
	 * @return a {@link Volume}, computed by <code>p</code>
	 */
	public Volume get(Object object, boolean asNode, Polygonizable p) {
		gs.setObjectContext(object, asNode);
		ContextDependent cd = p.getPolygonizableSource(gs);
		if (cd == null) {
			return null;
		}
		return (Volume) getValue(object, asNode, cd, p.getPolygonization());
	}

	/**
	 * Returns the {@link Volume} of a {@link LineSegmentizable}
	 * in the given object context.
	 * 
	 * @param object the context
	 * @param asNode <code>true</code> if <code>object</code> is a node,
	 * <code>false</code> if <code>object</code> is an edge
	 * @param ls the {@link LineSegmentizable} object
	 * @return a {@link Volume}, computed by <code>ls</code>
	 */
	public Volume get(Object object, boolean asNode, LineSegmentizable ls) {
		System.out.println("LineSegmentizable requested from cache");
		gs.setObjectContext(object, asNode);
		ContextDependent cd = ls.getSegmentizableSource(gs);
		if (cd == null) {
			return null;
		}
		return (Volume) getValue(object, asNode, cd, ls);
	}

	@Override
	protected Entry createEntry(Object obj, boolean node, ContextDependent dep, Object strategy) {
		return new Entry (obj, node, dep, strategy, gs) {
			@Override
			protected Object computeValue(Object oldValue, ContextDependent dependent, Object strategy, GraphState s) {
				System.out.println("Creating new volume for cache");
				if (strategy instanceof Polygonization) {
					PolygonArray out;
					// TODO: reuse old polygon array
					out = new PolygonArray ();
					((Polygonization) strategy).polygonize(dependent, s, out, flags, flatness);
					return new Polygons(out);
				} else if (strategy instanceof LineSegmentizable) {
					LineArray out;
					// TODO: reuse old line array
					out = new LineArray();
					((LineSegmentizable) strategy).segmentize(dependent, s, out, VolumeCache.this.flatness);
					return new Lines(out);
				} else {
					return null;
				}
			}	
		};
	}
}
