package me.mlata.gl4;

import com.jogamp.opengl.GL4;

/**
 * An enum representing one of the primitive types
 * supported in OpenGL.
 */
public enum Primitive {
    /**
     * A list of triangles. The next three vertices make up the next triangle.
     */
    Triangles,
    /**
     * A strip of triangles. The last two and the next vertex make up the next triangle.
     */
    TriangleStrip,
    /**
     * A fan of triangles. The first and the next two vertices make up the next triangle.
     */
    TriangleFan,
    /**
     * A list of lines. The next two vertices make up the next line.
     */
    Lines,
    /**
     * A strip of lines. The previous and the next vertex make up the next line.
     */
    LineStrip,
    /**
     * A loop of lines. A LineStrip where the first and the last vertex are connected.
     */
    LineLoop,
    /**
     * A list of points. Each vertex is a point.
     */
    Points;

    /**
     * Converts this enum to a OpenGL constant to be used in API function calls.
     * @return OpenGL constant corresponding to this primitive
     */
    public int toGLValue() {
        switch (this) {
            case Triangles:
                return GL4.GL_TRIANGLES;
            case TriangleStrip:
                return GL4.GL_TRIANGLE_STRIP;
            case TriangleFan:
                return GL4.GL_TRIANGLE_FAN;
            case Lines:
                return GL4.GL_LINES;
            case LineStrip:
                return GL4.GL_LINE_STRIP;
            case LineLoop:
                return GL4.GL_LINE_LOOP;
            case Points:
                return GL4.GL_POINTS;
            default:
                return 0;
        }
    }
}
