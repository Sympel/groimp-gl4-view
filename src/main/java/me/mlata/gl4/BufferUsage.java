package me.mlata.gl4;

import com.jogamp.opengl.GL4;

/**
 * An enum representing the possible buffer usage values in OpenGL. This does not
 * affect functionality, but may be used for optimization purposes by the driver.
 */
public enum BufferUsage {
    STREAM_DRAW,
    STREAM_READ,
    STREAM_COPY,
    STATIC_DRAW,
    STATIC_READ,
    STATIC_COPY,
    DYNAMIC_DRAW,
    DYNAMIC_READ,
    DYNAMIC_COPY;

    public int toGlValue() {
        switch (this) {
            case STREAM_DRAW:
                return GL4.GL_STREAM_DRAW;
            case STREAM_READ:
                return GL4.GL_STREAM_READ;
            case STREAM_COPY:
                return GL4.GL_STREAM_COPY;
            case STATIC_DRAW:
                return GL4.GL_STATIC_DRAW;
            case STATIC_READ:
                return GL4.GL_STATIC_READ;
            case STATIC_COPY:
                return GL4.GL_STATIC_COPY;
            case DYNAMIC_DRAW:
                return GL4.GL_DYNAMIC_DRAW;
            case DYNAMIC_READ:
                return GL4.GL_DYNAMIC_READ;
            case DYNAMIC_COPY:
                return GL4.GL_DYNAMIC_COPY;
            default:
                // Unreachable
                return 0;
        }
    }
}
