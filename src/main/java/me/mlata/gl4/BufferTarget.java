package me.mlata.gl4;

import com.jogamp.opengl.GL4;

/**
 * Represents the possible binding targets for OpenGL buffers.
 */
public enum BufferTarget {
    ARRAY,
    ATOMIC_COUNTER,
    COPY_READ,
    COPY_WRITE,
    DISPATCH_INDIRECT,
    DRAW_INDIRECT,
    ELEMENT_ARRAY,
    PIXEL_PACK,
    PIXEL_UNPACK,
    QUERY,
    SHADER_STORAGE,
    TEXTURE,
    TRANSFORM_FEEDBACK,
    UNIFORM;

    public int toGLValue() {
        switch (this) {
            case ARRAY:
                return GL4.GL_ARRAY_BUFFER;
            case ATOMIC_COUNTER:
                return GL4.GL_ATOMIC_COUNTER_BUFFER;
            case COPY_READ:
                return GL4.GL_COPY_READ_BUFFER;
            case COPY_WRITE:
                return GL4.GL_COPY_WRITE_BUFFER;
            case DISPATCH_INDIRECT:
                return GL4.GL_DISPATCH_INDIRECT_BUFFER;
            case DRAW_INDIRECT:
                return GL4.GL_DRAW_INDIRECT_BUFFER;
            case ELEMENT_ARRAY:
                return GL4.GL_ELEMENT_ARRAY_BUFFER;
            case PIXEL_PACK:
                return GL4.GL_PIXEL_PACK_BUFFER;
            case PIXEL_UNPACK:
                return GL4.GL_PIXEL_UNPACK_BUFFER;
            case QUERY:
                return GL4.GL_QUERY_BUFFER;
            case SHADER_STORAGE:
                return GL4.GL_SHADER_STORAGE_BUFFER;
            case TEXTURE:
                return GL4.GL_TEXTURE_BUFFER;
            case TRANSFORM_FEEDBACK:
                return GL4.GL_TRANSFORM_FEEDBACK_BUFFER;
            case UNIFORM:
                return GL4.GL_UNIFORM_BUFFER;
            default:
                return 0;
        }
    }
}
