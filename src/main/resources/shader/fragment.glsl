#version 460

#define PI 3.1415926535897932384626433832795

// position in world space
in vec3 position;
in vec3 normal;
in vec3 color;

layout(location = 0) uniform mat4 view;
layout(location = 2) uniform int light_count;
layout(location = 3) uniform float max_light_density;
layout(location = 4) uniform bool shading_enabled;

const int MAX_LIGHT_COUNT = 512;
const int POINT_LIGHT = 0;
const int SPOT_LIGHT = 1;
const int DIRECTIONAL_LIGHT = 2;

// lights are split into two seperate blocks to allow up to twice as many lights
struct LightTransform {
    // the position of the light in world space
    vec4 position;
    // the direction the light is pointing to
    vec4 orientation;
};
struct LightData {
    // pointlight = 0, spotlight = 1, directional light = 2
    int kind;
    // radiant power in watts
    float power;
    // inner angle for spot lights
    float inner_angle;
    // outer angle for spot lights
    float outer_angle;
    // color of the light
    vec3 color;
};
layout(std140, binding = 4) uniform LightsTransformBlock {
    LightTransform transforms[512];
} lightTransforms;
layout(std140, binding = 5) uniform LightsDataBlock {
    LightData data[512];
} lightData;

out vec4 f_color;

float luminance(vec3 v) {
    return dot(v, vec3(0.2126, 0.7152, 0.0722));
}

vec3 change_luminance(vec3 c_in, float l_out) {
    float l_in = luminance(c_in);
    return c_in * (l_out / l_in);
}

vec3 reinhard_extended_luminance(vec3 v, float max_white_l) {
    float l_old = luminance(v);
    float numerator = l_old * (1.0 + (l_old / (max_white_l * max_white_l)));
    float l_new = numerator / (1.0 + l_old);
    return change_luminance(v, l_new);
}

vec3 calculatePhong(vec3 light, vec3 view, vec3 normal, vec3 lightColor, vec3 color, float shininess, float power, float distance_factor) {
    // calculate halfway vector
    vec3 halfway = normalize(light + view);

    // calculate diffuse component
    float diff = max(dot(normal, light), 0.0);
    vec3 diffuse = (0.98 * diff + 0.05) * lightColor * color * power;

    // calculate specular component
    float spec = pow(max(dot(normal, halfway), 0.0), shininess);
    vec3 specular = spec * lightColor * power;

    return (diffuse + specular) * distance_factor;
}

void main() {
    if (!shading_enabled) {
        f_color = vec4(color, 1);
        return;
    }

    vec3 light = vec3(0.0);

    // TODO: pass this as uniform
    vec3 cam_position = inverse(view)[3].xyz;

    vec3 lightVec;
    vec3 viewDir = normalize(cam_position - position);
    float shininess = 12.0;
    // TODO: implement tone mapping as post processing step and handle distance again
    float distance;
    float distance_factor = 1;

    vec3 lightDir;
    vec3 phong;

    LightData[] ldata = lightData.data;
    LightTransform[] lts = lightTransforms.transforms;

    for (int i = 0; i < MAX_LIGHT_COUNT; i++) {
        if (i >= light_count) {
            break;
        }

        switch (ldata[i].kind) {
            case POINT_LIGHT:
                lightVec = vec3(lts[i].position) - position;
                // calculate direction of surface to light
                lightDir = normalize(lightVec);
                // calculate distance to light
                distance = length(lightVec);
                distance_factor = 1/(4 * PI) * 2;
                break;
            case SPOT_LIGHT:
                // TODO: add support for spot lights
                break;
            case DIRECTIONAL_LIGHT:
                // calculate direction of light to surface
                lightDir = -normalize(vec3(lts[i].orientation));
                break;
            default:
                break;
        }

        // calculate light reflected from light source
        phong = calculatePhong(lightDir, viewDir, normal, ldata[i].color, color, shininess, ldata[i].power, distance_factor);

        // add light to total light
        light += phong;
    }

    vec3 fragColor = reinhard_extended_luminance(light / 4, max_light_density);
    f_color = vec4(fragColor, 1.0);
}
