{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = { self, nixpkgs }: 
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        config.allowUnfree = true;
      };

      version = "1.0.0";

      buildInputs = with pkgs; [ jdk17 libxslt xorg.libXxf86vm steam-run ];

      nativeBuildInputs = with pkgs; [ makeWrapper maven ];

      testPlugin = pkgs.writeShellScriptBin "run-tests"
        ''
        steam-run mvn test
        '';

      buildPlugin = pkgs.writeShellScriptBin "package"
        ''
        steam-run mvn package
        '';
    in {
      devShells.x86_64-linux.default = pkgs.mkShell {
        inherit buildInputs;

        nativeBuildInputs = nativeBuildInputs ++ [
          testPlugin
          buildPlugin
        ];

        shellHook = ''
          export LD_LIBRARY_PATH=${pkgs.xorg.libXxf86vm}/lib:$LD_LIBRARY_PATH
        '';
      };
    };
}
